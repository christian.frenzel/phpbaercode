<?php 

namespace ponci_berlin\phpbaercode\cose;

use CBOR\CBORObject;
use CBOR\TagObject as Base;
use CBOR\Utils;

class Encrypt0Tag extends Base
{
    public static function getTagId(): int
    {
        return 16;
    }

    public static function createFromLoadedData(int $additionalInformation, ?string $data, CBORObject $object): Base
    {
        return new self($additionalInformation, $data, $object);
    }

    public static function create(CBORObject $object): Base
    {
        return new self(16, null, $object);
    }

    public function getNormalizedData(bool $ignoreTags = false)
        
    {
        return Utils::hexToString($this->object->getValue());
    }
}
?>
