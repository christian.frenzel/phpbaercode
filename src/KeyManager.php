<?php

declare(strict_types=1);

namespace ponci_berlin\phpbaercode;

use CBOR\ListObject;
use CBOR\SignedIntegerObject;
use CBOR\TextStringObject;
use CBOR\UnsignedIntegerObject;
use CBOR\MapObject;
use CBOR\ByteStringObject;
use OTPHP\TOTP;
use phpseclib3\Crypt\EC;
use GuzzleHttp;
use GuzzleHttp\Exception\ClientException;

const keyTypeECDSA = 2;
const keyTypeSymmetric = 4;

const algorithmAESGCM128 = 1;
const algorithmES512 = -36;

const curveNISTP512 = 3;

class KeyManager
{
    /**
     * @throws \Exception
     */
    public static function submitNewKeys(int $credType, string $userID, string $password, string $OTPSecret, string $serverUrl): array
    {
        // Build authentication details
        $OTPPassword = TOTP::create(
            $OTPSecret,
            30,
            'sha256',
            8
        )->now();
        $authString = $userID . ':' . $password . ':' . $OTPPassword;

        // generate keys
        $encryptionKey = openssl_random_pseudo_bytes(16);
        $privateKey = wrapPHPSECLIB::createKey('nistp521');
        $point = $privateKey->getPoint();

        // build private key object
        $ecKey = new MapObject();
        $ecKey->add(UnsignedIntegerObject::create(1), UnsignedIntegerObject::create(keyTypeECDSA));
        $ecKey->add(UnsignedIntegerObject::create(3), SignedIntegerObject::create(algorithmES512));
        $ecKey->add(SignedIntegerObject::create(-3), new ByteStringObject($point[1]->toBytes(true)));
        $ecKey->add(SignedIntegerObject::create(-2), new ByteStringObject($point[0]->toBytes(true)));
        $ecKey->add(SignedIntegerObject::create(-1), UnsignedIntegerObject::create(curveNISTP512)); // put $this->KeyOps here if they ever get used

        // build the symmetric key object
        $symKey = new MapObject();
        $symKey->add(UnsignedIntegerObject::create(1), UnsignedIntegerObject::create(keyTypeSymmetric));
        $symKey->add(UnsignedIntegerObject::create(3), UnsignedIntegerObject::create(algorithmAESGCM128));
        $symKey->add(SignedIntegerObject::create(-1), new ByteStringObject($encryptionKey));

        // build the packaged request object
        $message = new MapObject();
        $message->add(new TextStringObject("CredType"), UnsignedIntegerObject::create($credType));
        $message->add(new TextStringObject("ECCKey"), $ecKey);
        $message->add(new TextStringObject("AesKey"), $symKey);

        $message = (string)$message;

        $client = new GuzzleHttp\Client();

        $response = "";

        // Create a POST request
        try {
            $response = $client->request(
                'POST',
                $serverUrl,
                [
                    'headers' => [
                    'AUTHORIZATION' => $authString,
                    ],
                    'body' => $message,
                ],
            );
        } catch (ClientException $e) {
            throw new \Exception((string)$e->getResponse()->getBody());
        } catch (\throwable $e) {
            throw new \Exception($e->getMessage());
        }

        $id = $response->getBody()->getContents();

        return array(
            'id' => $id,
            'encryptionKey' => $encryptionKey,
            'privateKey' => $privateKey,
        );

    }
}

// wrapPHPSECLIB waps the phpseclib3 EC PrivateKey object to allow us to extract the public key values
class wrapPHPSECLIB extends EC\PrivateKey
{
    function getPoint(): array {
        return $this->QA;
    }

    // coppied verbatim from phpseclib, I don not recommend editing

    /**
     * @throws \ReflectionException
     * @throws \SodiumException
     */
    public static function createKey($curve)
    {
        self::initialize_static_variables();

        if (!isset(self::$engines['PHP'])) {
            self::useBestEngine();
        }

        $curve = strtolower($curve);
        if (self::$engines['libsodium'] && $curve == 'ed25519' && function_exists('sodium_crypto_sign_keypair')) {
            $kp = sodium_crypto_sign_keypair();
            $privatekey = EC::loadFormat('libsodium', sodium_crypto_sign_secretkey($kp));
            $privatekey->curveName = 'Ed25519';
            return $privatekey;
        }

        $privatekey = new wrapPHPSECLIB;

        $curveName = $curve;
        if (preg_match('#(?:^curve|^ed)\d+$#', $curveName)) {
            $curveName = ucfirst($curveName);
        } elseif (substr($curveName, 0, 10) == 'brainpoolp') {
            $curveName = 'brainpoolP' . substr($curveName, 10);
        }
        $curve = '\phpseclib3\Crypt\EC\Curves\\' . $curveName;

        if (!class_exists($curve)) {
            throw new UnsupportedCurveException('Named Curve of ' . $curveName . ' is not supported');
        }

        $reflect = new \ReflectionClass($curve);
        $curveName = $reflect->isFinal() ?
            $reflect->getParentClass()->getShortName() :
            $reflect->getShortName();

        $curve = new $curve();
        $privatekey->dA = $dA = $curve->createRandomMultiplier();
        if ($curve instanceof Curve25519 && self::$engines['libsodium']) {
            //$r = pack('H*', '0900000000000000000000000000000000000000000000000000000000000000');
            //$QA = sodium_crypto_scalarmult($dA->toBytes(), $r);
            $QA = sodium_crypto_box_publickey_from_secretkey($dA->toBytes());
            $privatekey->QA = [$curve->convertInteger(new BigInteger(strrev($QA), 256))];
        } else {
            $privatekey->QA = $curve->multiplyPoint($curve->getBasePoint(), $dA);
        }
        $privatekey->curve = $curve;
        $privatekey->curveName = $curveName;

        if ($privatekey->curve instanceof TwistedEdwardsCurve) {
            return $privatekey->withHash($curve::HASH);
        }

        return $privatekey;
    }

}
