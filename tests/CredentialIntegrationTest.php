<?php

declare(strict_types=1);

use ponci_berlin\phpbaercode\Credential;
use PHPUnit\Framework\TestCase;
use ponci_berlin\phpbaercode\Procedure;

final class CredentialIntegrationTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testCredentialGeneration(): void
    {
        $procedure = new Procedure(
            1,
            DateTime::createFromFormat(DateTimeInterface::RFC3339, "2021-05-01T08:00:00Z"),
        );
        $procedures = array($procedure);
        $credential = new Credential(
            "Max",
            "Mustermann",
            DateTime::createFromFormat(DateTimeInterface::RFC3339, "1990-04-01T00:00:00Z"),
            $procedures,
            "PoNC Inc",
            false,
        );
        $cbor_encoded = $credential->encode_cbor();
        $scratch_dir = getenv("SCRATCH_DIR");
        file_put_contents("$scratch_dir/php_credential.cbor", bin2hex($cbor_encoded));
    }
}
