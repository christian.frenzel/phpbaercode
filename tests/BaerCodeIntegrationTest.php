<?php

declare(strict_types=1);

use ponci_berlin\phpbaercode\BaerCode;
use ponci_berlin\phpbaercode\Procedure;
use PHPUnit\Framework\TestCase;

use phpseclib3\Crypt\EC;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertInstanceOf;

final class BaerCodeIntegrationTest extends TestCase
{

    /**
     * @doesNotPerformAssertions
     */
    public function testBaerCodeGeneration(): void
    {
        $procedure = new Procedure(
            1,
            DateTime::createFromFormat(DateTimeInterface::RFC3339, "2021-05-01T08:00:00Z"),
        );
        $procedures = array($procedure);

        $privateKey = EC::createKey('nistp521');
        $first_name = "Max";
        $last_name = "Mustermann";
        $date_of_birth = DateTime::createFromFormat(DateTimeInterface::RFC3339, "1990-04-01T00:00:00Z");
        $operator = "PoNC GmbH";
        $kid = "aef231adb";
        $aesKey = openssl_random_pseudo_bytes(16);
        $bc = new BaerCode(
            $first_name,
            $last_name,
            $date_of_birth,
            $procedures,
            $operator,
            // The test result is negative, i.e., bool is false
            false,
            $kid,
            $privateKey,
            $aesKey
        );
        $baercode_b64 = $bc->generate();
        $baercode = $bc->generate_qr();
        $scratch_dir = getenv("SCRATCH_DIR");
        file_put_contents("$scratch_dir/php_baercode.b64", $baercode_b64);
        file_put_contents("$scratch_dir/php_baercode_cryptkey.hex", bin2hex($aesKey));
        file_put_contents("$scratch_dir/php_baercode_pubkey.pem", $privateKey->getPublicKey());
        file_put_contents("$scratch_dir/php_baercode.png", $baercode);

        // Add another baercode signed by malicious provider
        $privateKey = EC::createKey('nistp521');
        $bc = new BaerCode(
            $first_name,
            $last_name,
            $date_of_birth,
            $procedures,
            $operator,
            true,
            $kid,
            $privateKey,
            $aesKey
        );
        $baercode_b64 = $bc->generate();
        file_put_contents("$scratch_dir/php_baercode_malicious_key.b64", $baercode_b64);
    }

}
