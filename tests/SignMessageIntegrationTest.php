<?php

declare(strict_types=1);

use ponci_berlin\phpbaercode\cose\Encrypt0Message;
use ponci_berlin\phpbaercode\cose\Signature;
use ponci_berlin\phpbaercode\cose\SignMessage;
use PHPUnit\Framework\TestCase;

use phpseclib3\Crypt\EC;

final class SignMessageIntegrationTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testSignMessageGeneration(): void
    {
        $key = EC::createKey('nistp521');
        $publicKey = $key->getPublicKey();
        $msg = new SignMessage("test12356");
        $sig = new Signature($msg, $key, "aabbccddeeff");
        $msg->sign();
        $cbor_encoded = $msg->encode_cbor();
        $scratch_dir = getenv("SCRATCH_DIR");
        file_put_contents("$scratch_dir/php_signmessage.cbor", bin2hex($cbor_encoded));
        file_put_contents("$scratch_dir/php_publickey.pem", $publicKey);
    }
}
?>
